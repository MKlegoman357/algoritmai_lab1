# Algoritmai_lab1

## Task

- Implement two variants of two sorting algorithms: one using an array and one using a linked list.
- Implement a search algorithm.

Arrays and linked lists must be implemented in two ways: using RAM and file storage. All algorithms must work with both types of arrays and linked lists.

## Assigned algorithms

Sorting algorithms:
- Merge sort
- Counting sort (only implemented for integer values)

Search algorithm:
- based on hash table using open addressing, with quadratic probing.