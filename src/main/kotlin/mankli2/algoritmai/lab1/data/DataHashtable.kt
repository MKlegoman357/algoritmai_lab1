package mankli2.algoritmai.lab1.data

class DataHashtable<T>(initialArrayLength: Int = 100, private val arrayInit: (Int) -> DataArray<T?>) : Iterable<T> {
    var size: Int = 0
        private set

    private var array: DataArray<T?> = arrayInit(initialArrayLength)

    fun insert(value: T) {
        val index = hash(value)

        if (index == -1)
            throw IndexOutOfBoundsException(index)

        array[index] = value
        size++
    }

    fun search(value: T): Boolean {
        return array[hash(value)] != null
    }

    private fun hash(value: T): Int {
        val hash = Math.abs(value.hashCode()) % array.length
        var index = hash

        for (i in 0 until array.length) {
            val arrayValue = array[index]
            if (arrayValue == null || arrayValue == value) {
                return index
            }
            index = (hash + i * i) % array.length
        }

        return -1
    }

    override fun iterator(): Iterator<T> {
        return object : Iterator<T> {
            private var index: Int = -1

            init {
                findNextItem()
            }

            override fun hasNext(): Boolean {
                return index < array.length
            }

            override fun next(): T {
                val item = array[index]!!
                findNextItem()
                return item
            }

            private fun findNextItem() {
                index++

                for (i in index until array.length) {
                    if (array[i] != null) {
                        index = i
                        return
                    }
                }

                index = array.length
            }
        }
    }
}