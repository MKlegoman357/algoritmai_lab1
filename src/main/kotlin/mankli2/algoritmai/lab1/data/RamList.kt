package mankli2.algoritmai.lab1.data

import mankli2.algoritmai.lab1.data.DataList.Node

class RamList<T> : DataList<T, RamList.RamNode<T>> {
    override var length: Int = 0
        private set

    override var head: RamNode<T>? = null
    override var tail: RamNode<T>? = null

    override fun push(value: T) {
        if (head == null) {
            head = RamNode(value)
            tail = head
        } else {
            tail!!.next = RamNode(value)
            tail = tail!!.next
        }

        length++
    }

    class RamNode<T>(
        override var value: T,
        override var next: RamNode<T>? = null
    ) : Node<T, RamNode<T>>
}