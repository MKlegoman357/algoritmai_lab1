package mankli2.algoritmai.lab1.data

interface DataList<T, N : DataList.Node<T, N>> : Iterable<N> {
    val length: Int

    var head: N?
    var tail: N?

    fun push(value: T)

    override fun iterator(): Iterator<N> {
        return object : Iterator<N> {
            var current: N? = head

            override fun hasNext(): Boolean {
                return current != null
            }

            override fun next(): N {
                val node = current ?: throw NoSuchElementException()
                current = node.next
                return node
            }
        }
    }

    interface Node<T, N : Node<T, N>> {
        var value: T
        var next: N?
    }
}