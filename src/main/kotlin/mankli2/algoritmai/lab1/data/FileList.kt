package mankli2.algoritmai.lab1.data

import mankli2.algoritmai.lab1.data.serializers.DataSerializer
import java.io.Closeable
import java.io.File
import java.io.RandomAccessFile
import java.nio.ByteBuffer
import java.nio.channels.FileChannel

class FileList<T>(file: File, newFile: Boolean = true, private val serializer: DataSerializer<T>) :
    DataList<T, FileList.FileNode<T>>, Closeable {

    private val intSize = 4
    private val indexSize = 4
    private val valueSize: Int = serializer.byteLength

    override var length: Int = 0
        private set(value) {
            if (field != value) {
                field = value

                dataChannel.position(0)
                writeInt(value)
            }
        }

    override var head: FileNode<T>? = null
    override var tail: FileNode<T>? = null

    private val dataChannel: FileChannel = RandomAccessFile(file, "rw").channel

    private val intBuffer: ByteBuffer = ByteBuffer.allocateDirect(intSize)
    private val valueBuffer: ByteBuffer = ByteBuffer.allocateDirect(valueSize)
    private val nodeBuffer: ByteBuffer = ByteBuffer.allocateDirect(valueSize + indexSize)

    init {
        if (newFile) {
            dataChannel.truncate(0)
            writeInt(0)
        } else {
            length = readInt()
        }
    }

    override fun push(value: T) {
        if (head == null) {
            head = createNode(length, value)
            tail = head
        } else {
            val nextNode = createNode(length, value)
            tail!!.next = nextNode
            tail = nextNode
        }

        length++
    }

    override fun close() {
        dataChannel.close()
    }

    private fun getPosition(index: Int): Long {
        return (intSize + (valueSize + indexSize) * index).toLong()
    }

    private fun readInt(): Int {
        intBuffer.position(0)
        if (dataChannel.read(intBuffer) == -1) return 0
        return intBuffer.getInt(0)
    }

    private fun writeInt(value: Int) {
        intBuffer.putInt(0, value)
        intBuffer.position(0)
        dataChannel.write(intBuffer)
    }

    private fun createNode(index: Int, value: T): FileNode<T> {
        serializer.writeValue(nodeBuffer, 0, value)
        nodeBuffer.putInt(valueSize, -1)
        nodeBuffer.position(0)
        dataChannel.write(nodeBuffer, getPosition(index))

        return FileNode(this, index, value)
    }

    private fun readValue(index: Int): T {
        valueBuffer.position(0)
        if (dataChannel.read(valueBuffer, getPosition(index)) == -1) return serializer.defaultValue()
        return serializer.readValue(valueBuffer, 0)
    }

    private fun writeValue(index: Int, value: T) {
        serializer.writeValue(valueBuffer, 0, value)
        valueBuffer.position(0)
        dataChannel.write(valueBuffer, getPosition(index))
    }

    private fun readNextIndex(index: Int): Int {
        intBuffer.position(0)
        if (dataChannel.read(intBuffer, getPosition(index) + valueSize) == -1) return -1
        return intBuffer.getInt(0)
    }

    private fun writeNextIndex(index: Int, nextIndex: Int) {
        intBuffer.putInt(0, nextIndex)
        intBuffer.position(0)
        dataChannel.write(intBuffer, getPosition(index) + valueSize)
    }

    class FileNode<T>(
        private val fileList: FileList<T>,
        private val index: Int,
        value: T
    ) : DataList.Node<T, FileNode<T>> {
        private var nextIndex: Int
            get() = fileList.readNextIndex(index)
            set(value) = fileList.writeNextIndex(index, value)

        override var value: T = value
            set(value) {
                fileList.writeValue(index, value)
                field = value
            }

        override var next: FileNode<T>?
            get() {
                val nextIndex = nextIndex

                if (nextIndex == -1)
                    return null

                return FileNode(fileList, nextIndex, fileList.readValue(nextIndex))
            }
            set(value) {
                nextIndex = value?.index ?: -1
            }
    }
}