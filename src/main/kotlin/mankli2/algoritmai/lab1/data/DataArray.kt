package mankli2.algoritmai.lab1.data

interface DataArray<T> : Iterable<T> {
    val length: Int

    operator fun get(index: Int): T
    operator fun set(index: Int, value: T)

    fun copyTo(other: DataArray<T>, start: Int, end: Int, otherStart: Int) {
        for (index in start..end) {
            other[otherStart + index - start] = this[index]
        }
    }

    // default iterator, that will work with any implementation
    override fun iterator(): Iterator<T> {
        return object : Iterator<T> {
            var index = 0

            override fun hasNext(): Boolean {
                return index < length
            }

            override fun next(): T {
                if (index >= length)
                    throw IndexOutOfBoundsException()

                return get(index++)
            }
        }
    }
}