package mankli2.algoritmai.lab1.data

import mankli2.algoritmai.lab1.data.serializers.DataSerializer
import java.io.Closeable
import java.io.File
import java.io.RandomAccessFile
import java.nio.ByteBuffer
import java.nio.channels.FileChannel

class FileArray<T>(file: File, length: Int = -1, private val serializer: DataSerializer<T>) : DataArray<T>, Closeable {

    private val intSize = 4
    private val valueSize: Int = serializer.byteLength

    override val length: Int

    val dataChannel: FileChannel = RandomAccessFile(file, "rw").channel

    private val intBuffer: ByteBuffer = ByteBuffer.allocateDirect(intSize)
    private val valueBuffer: ByteBuffer = ByteBuffer.allocateDirect(valueSize)

    init {
        if (length == -1) { // will be reading the current data from the file
            this.length = readInt()
        } else { // will be creating the amount of data specified by 'length'
            dataChannel.truncate(0)
            writeInt(length)
            this.length = length
        }
    }

    override fun close() {
        dataChannel.close()
    }

    override fun copyTo(other: DataArray<T>, start: Int, end: Int, otherStart: Int) {
        if (other !is FileArray) {
            super.copyTo(other, start, end, otherStart)
            return
        }

        other.dataChannel.position(other.getPosition(otherStart))
        dataChannel.transferTo(getPosition(start), getPosition(end - start + 1), other.dataChannel)
    }

    override fun get(index: Int): T {
        if (index < 0 || index >= length)
            throw IndexOutOfBoundsException()

        return readValue(index)
    }

    override fun set(index: Int, value: T) {
        if (index < 0 || index >= length)
            throw IndexOutOfBoundsException()

        writeValue(index, value)
    }

    private fun getPosition(index: Int): Long {
        return (intSize + index * valueSize).toLong()
    }

    private fun readInt(): Int {
        intBuffer.position(0)
        if (dataChannel.read(intBuffer) == -1) return 0
        return intBuffer.getInt(0)
    }

    private fun writeInt(value: Int) {
        intBuffer.putInt(0, value)
        intBuffer.position(0)
        dataChannel.write(intBuffer)
    }

    private fun readValue(index: Int): T {
        valueBuffer.position(0)
        if (dataChannel.read(valueBuffer, getPosition(index)) == -1) return serializer.defaultValue()
        return serializer.readValue(valueBuffer, 0)
    }

    private fun writeValue(index: Int, value: T) {
        serializer.writeValue(valueBuffer, 0, value)
        valueBuffer.position(0)
        dataChannel.write(valueBuffer, getPosition(index))
    }
}