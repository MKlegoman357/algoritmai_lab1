package mankli2.algoritmai.lab1.data.serializers

import java.nio.ByteBuffer

class FloatSerializer : DataSerializer<Float> {

    override val byteLength: Int = 4

    override fun readValue(buffer: ByteBuffer, pos: Int): Float {
        return buffer.getFloat(pos)
    }

    override fun writeValue(buffer: ByteBuffer, pos: Int, value: Float) {
        buffer.putFloat(pos, value)
    }

    override fun defaultValue() = 0f
}