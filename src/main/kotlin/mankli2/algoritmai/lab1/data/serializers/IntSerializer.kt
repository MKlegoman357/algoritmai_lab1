package mankli2.algoritmai.lab1.data.serializers

import java.nio.ByteBuffer

class IntSerializer : DataSerializer<Int> {

    override val byteLength: Int = 4

    override fun readValue(buffer: ByteBuffer, pos: Int): Int {
        return buffer.getInt(pos)
    }

    override fun writeValue(buffer: ByteBuffer, pos: Int, value: Int) {
        buffer.putInt(pos, value)
    }

    override fun defaultValue() = 0
}