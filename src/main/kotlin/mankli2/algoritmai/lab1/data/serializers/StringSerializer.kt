package mankli2.algoritmai.lab1.data.serializers

import java.nio.ByteBuffer

class StringSerializer(private val maxStringLength: Int = 64) : DataSerializer<String> {
    override val byteLength: Int = 4 + maxStringLength * 2

    override fun readValue(buffer: ByteBuffer, pos: Int): String {
        val length = buffer.getInt(pos)
        if (length <= 0) return ""
        val charArray = CharArray(length)
        buffer.position(pos + 4)
        buffer.asCharBuffer().get(charArray)
        return String(charArray)
    }

    override fun writeValue(buffer: ByteBuffer, pos: Int, value: String) {
        if (value.length > maxStringLength) throw IllegalArgumentException("String too big: ${value.length}")
        buffer.putInt(pos, value.length)
        buffer.position(pos + 4)
        buffer.asCharBuffer().put(value)
    }

    override fun defaultValue(): String  = ""
}