package mankli2.algoritmai.lab1.data.serializers

import java.nio.ByteBuffer

interface DataSerializer<T> {

    val byteLength: Int

    fun readValue(buffer: ByteBuffer, pos: Int): T
    fun writeValue(buffer: ByteBuffer, pos: Int, value: T)
    fun defaultValue(): T

}