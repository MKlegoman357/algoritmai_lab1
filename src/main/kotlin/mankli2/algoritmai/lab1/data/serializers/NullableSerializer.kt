package mankli2.algoritmai.lab1.data.serializers

import java.nio.ByteBuffer

class NullableSerializer<T>(private val serializer: DataSerializer<T>) : DataSerializer<T?> {
    override val byteLength: Int = 1 + serializer.byteLength

    override fun readValue(buffer: ByteBuffer, pos: Int): T? {
        val isNull: Byte = buffer.get(pos)
        if (isNull == 0.toByte()) return null
        return serializer.readValue(buffer, pos + 1)
    }

    override fun writeValue(buffer: ByteBuffer, pos: Int, value: T?) {
        buffer.put(pos, if (value == null) 0 else 1)
        if (value != null) serializer.writeValue(buffer, pos + 1, value)
    }

    override fun defaultValue(): T? = null
}