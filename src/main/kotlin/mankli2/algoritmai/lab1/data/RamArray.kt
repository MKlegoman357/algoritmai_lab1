package mankli2.algoritmai.lab1.data

class RamArray<T>(private val data: Array<T>) : DataArray<T> {

    override val length: Int = data.size

    override fun get(index: Int): T {
        if (index < 0 || index >= length)
            throw IndexOutOfBoundsException(index)

        return data[index]
    }

    override fun set(index: Int, value: T) {
        if (index < 0 || index >= length)
            throw IndexOutOfBoundsException(index)

        data[index] = value
    }
}

@Suppress("UNCHECKED_CAST", "FunctionName")
inline fun <reified T> RamArray(length: Int): RamArray<T> =
    RamArray(java.lang.reflect.Array.newInstance(T::class.java, length) as Array<T>)