package mankli2.algoritmai.lab1.timing

class Timer {
    private var start: Long = 0
    private var end: Long = 0

    fun start(): Timer {
        start = System.nanoTime()
        return this
    }

    fun stop(): Long {
        end = System.nanoTime()

        return end - start
    }

    fun current(): Long {
        return System.nanoTime() - start
    }
}