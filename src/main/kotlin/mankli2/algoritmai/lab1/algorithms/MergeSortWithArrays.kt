package mankli2.algoritmai.lab1.algorithms

import mankli2.algoritmai.lab1.data.DataArray

/**
 * Sorts the data using merge sort algorithm and a work array. Both arrays must be the same length.
 *
 * Adapted from https://en.wikipedia.org/wiki/Merge_sort Top-down implementation.
 */
class MergeSortWithArrays {
    fun <T : Comparable<T>> sort(array: DataArray<T>, workArray: DataArray<T>) {
        array.copyTo(workArray, 0, array.length - 1, 0)
        mergeSplit(workArray, array, 0, array.length)
    }

    private fun <T : Comparable<T>> mergeSplit(array: DataArray<T>, workArray: DataArray<T>, left: Int, right: Int) {
        if (left + 1 < right) {
            val middle = (left + right) / 2

            mergeSplit(workArray, array, left, middle)
            mergeSplit(workArray, array, middle, right)

            merge(array, workArray, left, middle, right)
        }
    }

    private fun <T : Comparable<T>> merge(array: DataArray<T>, workArray: DataArray<T>, left: Int, middle: Int, right: Int) {
        var i = left
        var j = middle

        for (index in left until right) {
            if (i < middle && (j >= right || array[i] <= array[j])) {
                workArray[index] = array[i]
                i++
            } else {
                workArray[index] = array[j]
                j++
            }
        }
    }
}