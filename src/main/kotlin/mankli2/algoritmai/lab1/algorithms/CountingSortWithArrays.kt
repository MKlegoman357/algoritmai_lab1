package mankli2.algoritmai.lab1.algorithms

import mankli2.algoritmai.lab1.data.DataArray

/**
 * Adapted from https://www.geeksforgeeks.org/counting-sort/
 */
class CountingSortWithArrays {
    fun sort(array: DataArray<Int>, workArray: DataArray<Int>, countArrayInit: (length: Int) -> DataArray<Int>) {
        if (array.length < 2) return

        val min = min(array)
        val max = max(array)
        val range = max - min + 1

        if (range < 1)
            throw IllegalArgumentException("The range of values must be lower or equal to Int.MAX_VALUE")

        val count = countArrayInit(range)

        for (i in 0 until array.length)
            count[array[i] - min]++
        for (i in 1 until range)
            count[i] += count[i - 1]
        for (i in array.length - 1 downTo 0) {
            val value = array[i]
            val countOfValue = count[value - min]
            workArray[countOfValue - 1] = value
            count[value - min] = countOfValue - 1
        }

        workArray.copyTo(array, 0, workArray.length - 1, 0)
    }

    private fun min(array: DataArray<Int>): Int {
        var min: Int = Int.MAX_VALUE

        for (item in array) {
            if (item < min) {
                min = item
            }
        }

        return min
    }

    private fun max(array: DataArray<Int>): Int {
        var max: Int = Int.MIN_VALUE

        for (item in array) {
            if (item > max) {
                max = item
            }
        }

        return max
    }
}