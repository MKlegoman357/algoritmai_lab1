package mankli2.algoritmai.lab1.algorithms

import mankli2.algoritmai.lab1.data.DataArray
import mankli2.algoritmai.lab1.data.DataList

/**
 * Adapted from http://www.geekviewpoint.com/java/singly_linked_list/counting_sort
 */
class CountingSortWithLists {
    fun <N : DataList.Node<Int, N>> sort(list: DataList<Int, N>, countArrayInit: (length: Int) -> DataArray<Int>) {
        if (list.length < 2) return

        val min = min(list)
        val max = max(list)
        val range = max - min + 1

        if (range < 1)
            throw IllegalArgumentException("The range of values must be lower or equal to Int.MAX_VALUE")

        val count = countArrayInit(range)

        for (node in list)
            count[node.value - min]++

        var node = list.head
        var tail = node
        for (i in 0 until count.length) {
            val value = i + min
            for (j in 0 until count[i]) {
                node!!.value = value
                tail = node
                node = node.next
            }
        }

        list.tail = tail
    }

    private fun <N : DataList.Node<Int, N>> min(list: DataList<Int, N>): Int {
        var min: Int = Int.MAX_VALUE

        for (item in list) {
            if (item.value < min) {
                min = item.value
            }
        }

        return min
    }

    private fun <N : DataList.Node<Int, N>> max(list: DataList<Int, N>): Int {
        var max: Int = Int.MIN_VALUE

        for (item in list) {
            if (item.value > max) {
                max = item.value
            }
        }

        return max
    }
}