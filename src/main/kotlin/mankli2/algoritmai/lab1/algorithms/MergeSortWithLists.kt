package mankli2.algoritmai.lab1.algorithms

import mankli2.algoritmai.lab1.data.DataList
import mankli2.algoritmai.lab1.data.DataList.Node

/**
 * Adapted from https://www.geeksforgeeks.org/merge-sort-for-linked-list/
 */
class MergeSortWithLists {
    fun <T : Comparable<T>, N : Node<T, N>> sort(list: DataList<T, N>) {
        list.head = mergeSort(list.head, list, true)
    }

    private fun <T : Comparable<T>, N : Node<T, N>> mergeSort(head: N?, list: DataList<T, N>, setTail: Boolean = false): N? {
        if (head?.next == null)
            return head

        val middle = getMiddle(head, list, setTail)
        val nextOfMiddle = middle.next

        middle.next = null

        val left = mergeSort(head, list)
        val right = mergeSort(nextOfMiddle, list)

        return merge(left, right)
    }

    private fun <T, N : Node<T, N>> getMiddle(head: N, list: DataList<T, N>, setTail: Boolean): N {
        var tail = head
        var fastNode = head.next
        var slowNode = head

        while (fastNode != null) {
            tail = fastNode
            fastNode = fastNode.next

            if (fastNode != null) {
                slowNode = slowNode.next!!
                tail = fastNode
                fastNode = fastNode.next
            }
        }

        if (setTail)
            list.tail = tail

        return slowNode
    }

    private fun <T : Comparable<T>, N : Node<T, N>> merge(left: N?, right: N?): N? {
        var head: N? = null
        var tail: N? = null

        var leftNode = left
        var rightNode = right

        while (leftNode != null || rightNode != null) {
            if (leftNode != null && (rightNode == null || leftNode.value <= rightNode.value)) {
                if (tail == null) {
                    head = leftNode
                    tail = head
                } else {
                    tail.next = leftNode
                    tail = leftNode
                }

                leftNode = leftNode.next
            } else {
                if (tail == null) {
                    head = rightNode
                    tail = head
                } else {
                    tail.next = rightNode
                    tail = rightNode
                }

                rightNode = rightNode!!.next
            }
        }

        return head
    }
}