package mankli2.algoritmai.lab1

import com.importre.crayon.blue
import com.importre.crayon.green
import com.importre.crayon.red
import mankli2.algoritmai.lab1.algorithms.CountingSortWithArrays
import mankli2.algoritmai.lab1.algorithms.CountingSortWithLists
import mankli2.algoritmai.lab1.algorithms.MergeSortWithArrays
import mankli2.algoritmai.lab1.algorithms.MergeSortWithLists
import mankli2.algoritmai.lab1.data.*
import mankli2.algoritmai.lab1.data.serializers.*
import mankli2.algoritmai.lab1.timing.Timer
import java.io.File
import java.util.*

const val dataFile = "data.bin"
const val workFile = "work.bin"
const val workFile2 = "work2.bin"

fun main() {
    /*val serializer = FloatSerializer()
    val cachedData: MutableMap<Int, Array<Float>> = mutableMapOf()
    val dataGenerator = { size: Int ->
        if (!cachedData.containsKey(size))
            cachedData[size] = generateFloatArray(size, 0f, 1f)
        cachedData[size]!!
    }*/

    val serializer = IntSerializer()
    val cachedData: MutableMap<Int, Array<Int>> = mutableMapOf()
    val dataGenerator = { size: Int ->
        if (!cachedData.containsKey(size)) {
            val array = generateIntArray(size, 0, 1000)
            array[0] = 0 // make sure the range of values is always the same
            array[size - 1] = 1000
            cachedData[size] = array
        }
        cachedData[size]!!
    }

    testAllMergeSort(serializer, dataGenerator)
    testAllCountingSort(serializer, dataGenerator)

    val hashtableSerializer = NullableSerializer(StringSerializer())
    val hashtableCachedData: MutableMap<Int, Array<String>> = mutableMapOf()
    val hashtableDataGenerator = { size: Int ->
        if (!hashtableCachedData.containsKey(size))
            hashtableCachedData[size] = generateStringArray(size, 1, 10)
        hashtableCachedData[size]!!
    }

    testAllHashtable(hashtableSerializer, hashtableDataGenerator)

    clearTestFiles()
}

inline fun <reified T> testAllHashtable(serializer: DataSerializer<T?>, noinline generateData: (size: Int) -> Array<T>) {
    testAlgorithm("hashtable search with quadratic probing", "RAM", null, generateData) {
        testHashtable(false, it, serializer)
    }

    testAlgorithm("hashtable search with quadratic probing", "file", null, generateData) {
        testHashtable(true, it, serializer)
    }
}

inline fun <reified T : Comparable<T>> testAllMergeSort(
    serializer: DataSerializer<T>,
    noinline generateData: (size: Int) -> Array<T>
) {
    testAlgorithm("merge sort", "RAM", "arrays", generateData) {
        testMergeSortWithArrays(false, it, serializer)
    }

    testAlgorithm("merge sort", "file", "arrays", generateData) {
        testMergeSortWithArrays(true, it, serializer)
    }

    testAlgorithm("merge sort", "RAM", "lists", generateData) {
        testMergeSortWithLists(false, it, serializer)
    }

    testAlgorithm("merge sort", "file", "lists", generateData) {
        testMergeSortWithLists(true, it, serializer)
    }
}

fun testAllCountingSort(serializer: DataSerializer<Int>, generateData: (size: Int) -> Array<Int>) {
    testAlgorithm("counting sort", "RAM", "arrays", generateData) {
        testCountingSortWithArrays(false, it, serializer)
    }

    testAlgorithm("counting sort", "file", "arrays", generateData) {
        testCountingSortWithArrays(true, it, serializer)
    }

    testAlgorithm("counting sort", "RAM", "lists", generateData) {
        testCountingSortWithLists(false, it, serializer)
    }

    testAlgorithm("counting sort", "file", "lists", generateData) {
        testCountingSortWithLists(true, it, serializer)
    }
}

fun <T> testAlgorithm(
    algorithm: String,
    storage: String,
    dataType: String?,
    generateData: (size: Int) -> Array<T>,
    test: (data: Array<T>) -> Long
) {
    println("Testing ${algorithm.red()} using ${storage.red()} storage${if (dataType != null) " and ${dataType.red()}" else ""}.")
    println()
    printfln("%7s | %13s", "# items", "duration (ms)")

    for (i in 0..5) {
        val itemCount = (1000 * Math.pow(2.0, i.toDouble())).toInt()
        val randomDataArray = generateData(itemCount)

        printf("${"%,7d".green()}   ", itemCount)

        var duration = 0L
        var testCount = 0
        val totalTimer = Timer().start()

        do {
            duration += test(randomDataArray)
            testCount++
        } while (duration < 1_000_000_000 && totalTimer.current() < 2_000_000_000) // don't let the tests run more than two seconds, if possible

        printfln("%,13.6f".blue(), duration / testCount / 1000000.0)
    }

    println()
}

inline fun <reified T : Comparable<T>> testMergeSortWithArrays(
    useFiles: Boolean = false,
    dataArray: Array<T>,
    serializer: DataSerializer<T>
): Long {
    val array: DataArray<T>
    val workArray: DataArray<T>

    if (useFiles) {
        array = FileArray(File(dataFile), dataArray.size, serializer)
        workArray = FileArray(File(workFile), dataArray.size, serializer)
    } else {
        array = RamArray(dataArray.size)
        workArray = RamArray(dataArray.size)
    }

    for ((i, value) in dataArray.withIndex())
        array[i] = value

    val mergeSort = MergeSortWithArrays()
    val timer = Timer()

    timer.start()
    mergeSort.sort(array, workArray)
    val duration = timer.stop()

    if (array is FileArray) array.close()
    if (workArray is FileArray) workArray.close()

    return duration
}

@Suppress("UNCHECKED_CAST")
fun <T : Comparable<T>> testMergeSortWithLists(
    useFiles: Boolean = false,
    dataArray: Array<T>,
    serializer: DataSerializer<T>
): Long {
    val list: DataList<T, *>

    if (useFiles) {
        list = FileList(File(dataFile), true, serializer)
    } else {
        list = RamList()
    }

    for (value in dataArray)
        list.push(value)

    val mergeSort = MergeSortWithLists()
    val timer = Timer()
    val duration: Long

    if (useFiles) { // should be a better way to implement this
        timer.start()
        mergeSort.sort(list as DataList<T, FileList.FileNode<T>>)
        duration = timer.stop()
    } else {
        timer.start()
        mergeSort.sort(list as DataList<T, RamList.RamNode<T>>)
        duration = timer.stop()
    }

    if (list is FileList) list.close()

    return duration
}

fun testCountingSortWithArrays(
    useFiles: Boolean = false,
    dataArray: Array<Int>,
    serializer: DataSerializer<Int>
): Long {
    val array: DataArray<Int>
    val workArray: DataArray<Int>
    var countArray: DataArray<Int>? = null

    if (useFiles) {
        array = FileArray(File(dataFile), dataArray.size, serializer)
        workArray = FileArray(File(workFile), dataArray.size, serializer)
    } else {
        array = RamArray(dataArray.size)
        workArray = RamArray(dataArray.size)
    }

    for ((i, value) in dataArray.withIndex())
        array[i] = value

    val countingSort = CountingSortWithArrays()
    val timer = Timer()

    timer.start()
    countingSort.sort(array, workArray) {
        val newArray: DataArray<Int>

        if (useFiles) {
            newArray = FileArray(File(workFile2), it, serializer)
        } else {
            newArray = RamArray(it)
        }

        for (i in 0 until newArray.length)
            newArray[i] = 0

        countArray = newArray

        newArray
    }
    val duration = timer.stop()

    if (array is FileArray) array.close()
    if (workArray is FileArray) workArray.close()
    if (countArray is FileArray) (countArray as FileArray<Int>).close()

    return duration
}

@Suppress("UNCHECKED_CAST")
fun testCountingSortWithLists(
    useFiles: Boolean = false,
    dataArray: Array<Int>,
    serializer: DataSerializer<Int>
): Long {
    val list: DataList<Int, *>
    var countArray: DataArray<Int>? = null

    if (useFiles) {
        list = FileList(File(dataFile), true, serializer)
    } else {
        list = RamList()
    }

    for (value in dataArray)
        list.push(value)

    val countingSort = CountingSortWithLists()
    val timer = Timer()
    val duration: Long

    val countInit = { length: Int ->
        val newArray: DataArray<Int>

        if (useFiles) {
            newArray = FileArray(File(workFile), length, serializer)
        } else {
            newArray = RamArray(length)
        }

        for (i in 0 until newArray.length)
            newArray[i] = 0

        countArray = newArray

        newArray
    }

    if (useFiles) { // should be a better way to implement this
        timer.start()
        countingSort.sort(list as DataList<Int, FileList.FileNode<Int>>, countInit)
        duration = timer.stop()
    } else {
        timer.start()
        countingSort.sort(list as DataList<Int, RamList.RamNode<Int>>, countInit)
        duration = timer.stop()
    }

    if (list is FileList) list.close()
    if (countArray is FileArray) (countArray as FileArray<Int>).close()

    return duration
}

inline fun <reified T> testHashtable(
    useFiles: Boolean,
    dataArray: Array<T>, serializer: DataSerializer<T?>
): Long {
    var array: DataArray<T?>? = null

    val hashtable = DataHashtable(dataArray.size * 2) {
        if (useFiles) {
            array = FileArray(File(dataFile), it, serializer)
            array!!
        } else {
            RamArray(it)
        }
    }

    for (item in dataArray) {
        hashtable.insert(item)
    }

    val timer = Timer()
    var duration = 0L

    for (item in dataArray) {
        timer.start()
        hashtable.search(item)
        duration += timer.stop()
    }

    if (array is FileArray) {
        (array as FileArray<T?>).close()
    }

    return duration / dataArray.size
}

fun <T> printData(array: Array<T>, length: Int = 5) {
    println("Data (first $length elements):".red())

    for ((i, value) in array.withIndex()) {
        println("$i. $value")

        if (i + 1 >= length) break
    }

    println()
}

inline fun printf(format: String, vararg arguments: Any?) {
    System.out.printf(format, *arguments)
}

inline fun printfln(format: String, vararg arguments: Any?) {
    printf(format + "\n", *arguments)
}

fun clearTestFiles() {
    File(dataFile).delete()
    File(workFile).delete()
    File(workFile2).delete()
}

fun <T> DataArray<T>.print(start: Int = 0, end: Int = length - 1) {
    if (start < 0 || start > end)
        throw IndexOutOfBoundsException(start)
    if (end >= length)
        throw IndexOutOfBoundsException(end)

    for (i in start..end) {
        println("$i. ${this[i]}")
    }
}

fun <T, N : DataList.Node<T, N>> DataList<T, N>.print(start: Int = 0, end: Int = length - 1) {
    var node = head
    var index = 0
    while (node != null) {
        if (index in start..end)
            println("$index. ${node.value}")
        else if (index > end)
            break

        node = node.next
        index++
    }
}