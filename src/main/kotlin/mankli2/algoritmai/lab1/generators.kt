package mankli2.algoritmai.lab1

import kotlin.random.Random

fun generateIntArray(elementCount: Int = 5, min: Int = Int.MIN_VALUE, max: Int = Int.MAX_VALUE) = Array(elementCount) {
    Random.nextInt(min, if (max == Int.MAX_VALUE) max else max + 1)
}

fun generateFloatArray(elementCount: Int = 5, min: Float = Float.MIN_VALUE, max: Float = Float.MAX_VALUE) = Array(elementCount) {
    Random.nextDouble(min.toDouble(), max.toDouble()).toFloat()
}

fun generateStringArray(elementCount: Int = 5, minLength: Int = 0, maxLength: Int = 10) = Array(elementCount) {
    val chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJGLMNOPQRSTUVWXYZ"
    val stringBuilder = StringBuilder()
    for (i in 1..Random.nextInt(minLength, maxLength + 1))
        stringBuilder.append(chars.random())
    stringBuilder.toString()
}